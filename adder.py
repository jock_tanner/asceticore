import sys
from typing import Iterable, List

from nmigen import Cat, Elaboratable, Module, Signal
from nmigen.asserts import Assert
from nmigen.build import Platform

from helpers import run_test


def connect_bus(domain: object, dst: Iterable, src: Iterable):
    for d, s in zip(dst, src):
        domain += d.eq(s)


class PartialFullAdder(Elaboratable):
    """
    1-bit full adder minus ripple carry generation logic.
    """

    def __init__(self):

        self.a_in = Signal()  # operand
        self.b_in = Signal()  # operand
        self.c_in = Signal()  # carry

        self.g_out = Signal()  # carry generation
        self.p_out = Signal()  # carry propagation
        self.s_out = Signal()  # sum

    def elaborate(self, platform: Platform) -> Module:

        m = Module()
        m.d.comb += self.g_out.eq(self.a_in & self.b_in)
        m.d.comb += self.p_out.eq(self.a_in ^ self.b_in)
        m.d.comb += self.s_out.eq(self.c_in ^ self.p_out)
        return m


class OutputCarryCircuit(Elaboratable):
    """
    Ripple carry generation logic.
    """

    def __init__(self):

        self.c_in = Signal()
        self.p_in = Signal()
        self.g_in = Signal()

        self.c_out = Signal()

    def elaborate(self, platform: Platform) -> Module:

        m = Module()
        m.d.comb += self.c_out.eq((self.c_in & self.p_in) | self.g_in)
        return m


class CarryLookaheadAccelerator4(Elaboratable):
    """
    4-bit carry lookahead generator.
    """

    def __init__(self):

        self.c_in = Signal()
        self.p_in = Signal(4)
        self.g_in = Signal(4)

        self.c0_out = Signal()
        self.c1_out = Signal()
        self.c2_out = Signal()
        self.p_out = Signal()
        self.g_out = Signal()

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        m.d.comb += self.c0_out.eq(
            (self.c_in & self.p_in[0])
            | self.g_in[0]
        )
        m.d.comb += self.c1_out.eq(
            (self.c_in & self.p_in[0] & self.p_in[1])
            | (self.g_in[0] & self.p_in[1])
            | self.g_in[1]
        )
        m.d.comb += self.c2_out.eq(
            (self.c_in & self.p_in[0] & self.p_in[1] & self.p_in[2])
            | (self.g_in[0] & self.p_in[1] & self.p_in[2])
            | (self.g_in[1] & self.p_in[2])
            | self.g_in[2]
        )
        m.d.comb += self.g_out.eq(
            (self.g_in[0] & self.p_in[1] & self.p_in[2] & self.p_in[3])
            | (self.g_in[1] & self.p_in[2] & self.p_in[3])
            | (self.g_in[2] & self.p_in[3])
            | self.g_in[3]
        )
        m.d.comb += self.p_out.eq(
            self.p_in[0] & self.p_in[1] & self.p_in[2] & self.p_in[3]
        )

        return m


class CarryLookaheadAdder4(Elaboratable):
    """
    Four 1-bit partial full adders + 4-bit carry lookahead generator.
    """

    def __init__(self):

        self.a_in = Signal(4)
        self.b_in = Signal(4)
        self.c_in = Signal()

        self.s_out = Signal(4)
        self.g_out = Signal()  # carry generation
        self.p_out = Signal()  # carry propagation
        self.penult_c_out = Signal()  # carry from last but one digit

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        # create submodules
        pfas = [
            PartialFullAdder(),
            PartialFullAdder(),
            PartialFullAdder(),
            PartialFullAdder(),
        ]
        for i, pfa in enumerate(pfas):
            setattr(m.submodules, f'pfa{i}', pfa)

        m.submodules.acc = acc = CarryLookaheadAccelerator4()

        # connect data inputs
        connect_bus(m.d.comb, [x.a_in for x in pfas], self.a_in)
        connect_bus(m.d.comb, [x.b_in for x in pfas], self.b_in)

        # connect accelerator inputs
        m.d.comb += acc.g_in.eq(Cat(x.g_out for x in pfas))
        m.d.comb += acc.p_in.eq(Cat(x.p_out for x in pfas))
        m.d.comb += acc.c_in.eq(self.c_in)

        # connect carry inputs of partial full adders
        for pfa, src in zip(
            pfas, [self.c_in, acc.c0_out, acc.c1_out, acc.c2_out],
        ):
            m.d.comb += pfa.c_in.eq(src)

        # connect data outputs
        m.d.comb += self.s_out.eq(Cat(x.s_out for x in pfas))

        # connect other outputs
        m.d.comb += self.g_out.eq(acc.g_out)
        m.d.comb += self.p_out.eq(acc.p_out)
        m.d.comb += self.penult_c_out.eq(acc.c2_out)

        return m

    def ports(self) -> List[Signal]:
        return [
            # inputs
            self.a_in, self.b_in, self.c_in,

            # regular outputs
            self.s_out,

            # accelerator chaining outputs
            self.g_out, self.p_out, self.penult_c_out,
        ]


class CarryLookaheadAdder16(Elaboratable):
    """
    16-bit adder:

    - four 4-bit accelerated adders,
    - one 4-bit carry lookahead accelerator,
    - one 1-bit ripple carry circuit for the top carry output.

    Can produce carry, negative, and signed overflow signals for either
    full 16-bit word or lower 8 bits (byte).
    """

    def __init__(self):

        self.a_in = Signal(16)
        self.b_in = Signal(16)
        self.c_in = Signal()
        self.byte_in = Signal()  # 1 − word mode, 2 − byte mode

        self.s_out = Signal(16)
        self.c_out = Signal()
        self.v_out = Signal()
        self.n_out = Signal()

        self.c_byte_out = Signal()
        self.c_word_out = Signal()
        self.v_byte_out = Signal()
        self.v_word_out = Signal()

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        # create submodules
        cla4s = [
            CarryLookaheadAdder4(),
            CarryLookaheadAdder4(),
            CarryLookaheadAdder4(),
            CarryLookaheadAdder4(),
        ]
        for i, cla4 in enumerate(cla4s):
            setattr(m.submodules, f'cla4{i}', cla4)

        m.submodules.acc = acc = CarryLookaheadAccelerator4()
        m.submodules.oc = oc = OutputCarryCircuit()

        # connect data inputs
        for i, cla in enumerate(cla4s):
            m.d.comb += cla.a_in.eq(self.a_in[i * 4:i * 4 + 4])
            m.d.comb += cla.b_in.eq(self.b_in[i * 4:i * 4 + 4])

        # connect accelerator inputs
        m.d.comb += acc.c_in.eq(self.c_in)
        m.d.comb += acc.p_in.eq(Cat(x.p_out for x in cla4s))
        m.d.comb += acc.g_in.eq(Cat(x.g_out for x in cla4s))

        # connect carry sources to adders' carry inputs
        for cla, c in zip(
            cla4s,
            [self.c_in, acc.c0_out, acc.c1_out, acc.c2_out],
        ):
            m.d.comb += cla.c_in.eq(c)

        # connect ripple carry circuit inputs
        m.d.comb += oc.c_in.eq(acc.c2_out)
        m.d.comb += oc.g_in.eq(acc.g_out)
        m.d.comb += oc.p_in.eq(acc.p_out)

        # connect data outputs
        m.d.comb += self.s_out.eq(Cat(x.s_out for x in cla4s))

        # connect carry sources
        m.d.comb += self.c_word_out.eq(oc.c_out)
        m.d.comb += self.c_byte_out.eq(acc.c1_out)

        # create and connect overflow logic
        m.d.comb += self.v_byte_out.eq(cla4s[1].penult_c_out ^ acc.c1_out)
        m.d.comb += self.v_word_out.eq(cla4s[3].penult_c_out ^ oc.c_out)

        # select sources for output carry, negative, and overflow signals
        with m.If(self.byte_in):
            m.d.comb += self.c_out.eq(self.c_byte_out)
            m.d.comb += self.v_out.eq(self.v_byte_out)
            m.d.comb += self.n_out.eq(m.submodules.cla41.s_out[3])

        with m.Else():
            m.d.comb += self.c_out.eq(self.c_word_out)
            m.d.comb += self.v_out.eq(self.v_word_out)
            m.d.comb += self.n_out.eq(m.submodules.cla43.s_out[3])

        return m

    def ports(self) -> List[Signal]:
        return [
            self.a_in, self.b_in, self.c_in, self.byte_in,
            self.s_out, self.c_out, self.v_out, self.n_out,
        ]

    @classmethod
    def test_16bit(cls):

        m = Module()
        m.submodules.adder16 = adder16 = cls()

        m.d.comb += adder16.byte_in.eq(0)  # operate on words
        m.d.comb += Assert(
            adder16.s_out == (adder16.a_in + adder16.b_in + adder16.c_in)[:16]
        )
        m.d.comb += Assert(
            adder16.c_out == (adder16.a_in + adder16.b_in + adder16.c_in)[16]
        )
        m.d.comb += Assert(
            adder16.v_out == (
                adder16.a_in + adder16.b_in + adder16.c_in
            )[16] ^ (
                adder16.a_in[:15] + adder16.b_in[:15] + adder16.c_in
            )[15]
        )
        return run_test(
            __file__, m,
            ports=[adder16.a_in, adder16.b_in, adder16.c_in, adder16.s_out],
        )

    @classmethod
    def test_8bit(cls):

        m = Module()
        m.submodules.adder16 = adder16 = cls()

        m.d.comb += adder16.byte_in.eq(1)  # operate on byte
        m.d.comb += Assert(
            adder16.s_out == (adder16.a_in + adder16.b_in + adder16.c_in)[:16]
        )
        m.d.comb += Assert(
            adder16.c_out == (
                adder16.a_in[:8] + adder16.b_in[:8] + adder16.c_in[:8]
            )[8]
        )
        m.d.comb += Assert(
            adder16.v_out == (
                adder16.a_in[:8] + adder16.b_in[:8] + adder16.c_in
            )[8] ^ (
                adder16.a_in[:7] + adder16.b_in[:7] + adder16.c_in
            )[7]
        )
        return run_test(
            __file__, m,
            ports=[adder16.a_in, adder16.b_in, adder16.c_in, adder16.s_out],
        )


if __name__ == '__main__':

    for test in (
        CarryLookaheadAdder16.test_16bit,
        CarryLookaheadAdder16.test_8bit,
    ):
        result = test()
        if result != 0:
            sys.exit(result)
