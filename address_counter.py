from typing import List

from nmigen import Cat, Elaboratable, Module, Signal
from nmigen.back.pysim import Simulator
from nmigen.build import Platform
from nmigen.cli import main_parser

from adder import OutputCarryCircuit, PartialFullAdder


class FullAdder(Elaboratable):

    def __init__(self):
        self.a_in = Signal()
        self.b_in = Signal()
        self.c_in = Signal()

        self.s_out = Signal()
        self.c_out = Signal()

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        m.submodules.pfa = pfa = PartialFullAdder()
        m.submodules.cc = cc = OutputCarryCircuit()

        m.d.comb += pfa.a_in.eq(self.a_in)
        m.d.comb += pfa.b_in.eq(self.b_in)
        m.d.comb += pfa.c_in.eq(self.c_in)
        m.d.comb += cc.c_in.eq(self.c_in)
        m.d.comb += cc.g_in.eq(pfa.g_out)
        m.d.comb += cc.p_in.eq(pfa.p_out)
        m.d.comb += self.c_out.eq(cc.c_out)
        m.d.comb += self.s_out.eq(pfa.s_out)

        return m


class FullAdder2(Elaboratable):

    def __init__(self):
        self.a_in = Signal(2)
        self.b_in = Signal(2)
        self.c_in = Signal()

        self.s_out = Signal(2)
        self.c_out = Signal()

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        m.submodules.adder_0 = adder_0 = FullAdder()
        m.submodules.adder_1 = adder_1 = FullAdder()

        m.d.comb += adder_0.a_in.eq(self.a_in[0])
        m.d.comb += adder_1.a_in.eq(self.a_in[1])
        m.d.comb += adder_0.b_in.eq(self.b_in[0])
        m.d.comb += adder_1.b_in.eq(self.b_in[1])

        m.d.comb += adder_0.c_in.eq(self.c_in)
        m.d.comb += adder_1.c_in.eq(adder_0.c_out)

        m.d.comb += self.c_out.eq(adder_1.c_out)
        m.d.comb += self.s_out.eq(Cat(adder_0.s_out, adder_1.s_out))

        return m


class AdderSubtractor(Elaboratable):

    def __init__(self):

        self.a_in = Signal(2)
        self.b_in = Signal(2)
        self.subtract_in = Signal()
        self.en_in = Signal()

        self.s_out = Signal(2)
        self.c_out = Signal()

        self.reg = Signal(2)

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        m.submodules.adder = adder = FullAdder2()

        with m.If(self.subtract_in == 1):
            m.d.comb += adder.b_in.eq(~self.b_in)
            m.d.comb += adder.c_in.eq(1)
        with m.Else():
            m.d.comb += adder.b_in.eq(self.b_in)
            m.d.comb += adder.c_in.eq(0)

        with m.If(self.en_in == 1):
            m.d.comb += adder.a_in.eq(self.a_in)
        with m.Else():
            m.d.comb += adder.a_in.eq(self.reg)

        m.d.comb += self.s_out.eq(adder.s_out)
        m.d.comb += self.c_out.eq(adder.c_out)
        m.d.sync += self.reg.eq(adder.s_out)

        return m


class UpDownCounter(Elaboratable):
    """
    The real-life implementation may consist of JK triggers and some gates.
    By now I employ the same trick as in `latch.py`.

    https://www.electronics-tutorials.ws/counter/count_4.html
    """

    def __init__(self):

        self.data_in = Signal(14)
        self.count_in = Signal()
        self.down_in = Signal()
        self.en_in = Signal()

        self.data_out = Signal(14)

        self.reg = Signal(14)

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        data = Signal(14)

        with m.If(self.en_in == 1):

            with m.If(self.count_in == 1):
                with m.If(self.down_in == 1):
                    m.d.comb += data.eq(self.data_in - 1)
                with m.Else():
                    m.d.comb += data.eq(self.data_in + 1)
            with m.Else():
                m.d.comb += data.eq(self.data_in)

        with m.Else():

            with m.If(self.count_in == 1):
                with m.If(self.down_in == 1):
                    m.d.comb += data.eq(self.reg - 1)
                with m.Else():
                    m.d.comb += data.eq(self.reg + 1)
            with m.Else():
                m.d.comb += data.eq(self.reg)

        m.d.comb += self.data_out.eq(data)
        m.d.sync += self.reg.eq(data)

        return m

    def ports(self) -> List[Signal]:
        return [self.data_in, self.count_in, self.down_in, self.data_out]


class AddressCounter(Elaboratable):

    def __init__(self):

        self.addr_in = Signal(16)
        self.count_in = Signal()
        self.down_in = Signal()
        self.byte_in = Signal()
        self.en_in = Signal()

        self.addr_out = Signal(16)

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        m.submodules.adder = adder = AdderSubtractor()
        m.submodules.cnt = cnt = UpDownCounter()

        m.d.comb += adder.a_in.eq(self.addr_in[:2])
        m.d.comb += cnt.data_in.eq(self.addr_in[2:])

        with m.If(self.count_in == 1):
            with m.If(self.byte_in == 1):
                m.d.comb += adder.b_in.eq(1)
            with m.Else():
                m.d.comb += adder.b_in.eq(2)
        with m.Else():
            m.d.comb += adder.b_in.eq(0)

        m.d.comb += adder.subtract_in.eq(self.down_in)
        m.d.comb += cnt.down_in.eq(self.down_in)

        m.d.comb += adder.en_in.eq(self.en_in)
        m.d.comb += cnt.en_in.eq(self.en_in)
        with m.If(self.down_in == 1):
            m.d.comb += cnt.count_in.eq(~adder.c_out)
        with m.Else():
            m.d.comb += cnt.count_in.eq(adder.c_out)

        m.d.comb += self.addr_out.eq(Cat(adder.s_out, cnt.data_out))

        return m

    def ports(self) -> List[Signal]:
        return [
            self.addr_in, self.count_in, self.down_in, self.byte_in,
            self.addr_out,
        ]


if __name__ == '__main__':

    def process():
        yield cnt.addr_in.eq(3)
        yield cnt.count_in.eq(1)
        yield cnt.en_in.eq(1)
        yield
        yield cnt.count_in.eq(0)
        yield cnt.en_in.eq(0)
        yield
        yield
        yield cnt.count_in.eq(1)
        yield
        yield cnt.count_in.eq(0)
        yield
        yield
        yield cnt.count_in.eq(1)
        yield cnt.byte_in.eq(0)
        yield cnt.down_in.eq(1)
        yield
        yield
        yield
        yield
        yield
        yield
        yield
        yield cnt.down_in.eq(0)
        yield cnt.byte_in.eq(1)
        yield
        yield
        yield
        yield
        yield
        yield

    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.cnt = cnt = AddressCounter()

    sim = Simulator(m)
    sim.add_clock(1e-6, domain='sync')
    sim.add_sync_process(process)

    with sim.write_vcd(
        'address_counter.vcd', 'address_counter.gtkw', traces=cnt.ports(),
    ):
        sim.run()
