from dataclasses import dataclass
from typing import List

from nmigen import Cat, Elaboratable, Instance, Module, Signal
from nmigen.build import Platform


@dataclass
class AltPLLConfig:
    divide_by: int = 1
    multiply_by: int = 1
    duty_cycle: int = 50
    phase_shift: str = '0'  # in picoseconds


class ALTPLL(Elaboratable):

    def __init__(
        self, plls: List[AltPLLConfig], width: int = 5,
        input_freq0: int = 50_000_000, input_freq1: int = None,
    ):
        self.inclk0 = Signal()
        self.inclk1 = Signal()

        self._width = width
        self.clk_out = Signal(width)
        assert 1 <= len(plls) <= width, (
            f'Configure at least 1, at most {width} PLLs'
        )
        self._plls = plls
        self._input_freq0 = input_freq0
        self._input_freq1 = input_freq1

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        # basic parameters
        pll_params = {
            'p_BANDWIDTH_TYPE': 'AUTO',
            'p_COMPENSATE_CLOCK': 'CLK0',
            'p_OPERATION_MODE': 'NORMAL',
            'p_PLL_TYPE': 'AUTO',
            'p_WIDTH_CLOCK': self._width,
            'i_inclk': Cat(self.inclk0, self.inclk1),
            'p_INCLK0_INPUT_FREQUENCY': 1_000_000_000_000 // self._input_freq0,
            'p_PORT_INCLK0': 'PORT_USED',
        }

        # input frequencies
        if self._input_freq1 is not None:
            pll_params['p_INCLK1_INPUT_FREQUENCY'] = (
                1_000_000_000_000 // self._input_freq1
            )
            pll_params['p_PORT_INCLK1'] = 'PORT_USED'

        # output
        for i, pll in enumerate(self._plls):
            pll_params[f'p_CLK{i}_DIVIDE_BY'] = pll.divide_by
            pll_params[f'p_CLK{i}_MULTIPLY_BY'] = pll.multiply_by
            pll_params[f'p_CLK{i}_DUTY_CYCLE'] = pll.duty_cycle
            pll_params[f'p_CLK{i}_PHASE_SHIFT'] = pll.phase_shift
            pll_params[f'p_PORT_CLK{i}'] = 'PORT_USED'

        pll_params['o_clk'] = self.clk_out

        m.submodules.pll = Instance('altpll', **pll_params)

        return m
