from typing import List

from nmigen import Cat, Const, Elaboratable, Module, Repl, Signal
from nmigen.build import Platform

from common import Cond, ConstAddr, InstrMode, Reg
from instr_set import *
from latch import Latch


class Decoder(Elaboratable):

    def __init__(self):

        self.instr_in = Signal(16)
        self.en_in = Signal()

        self.op_code_out = Signal(10)
        self.b_out = Signal()
        self.mode_out = Signal(InstrMode)
        self.reg_main_out = Signal(4)
        self.reg_offhand_out = Signal(4)
        self.cond_out = Signal(Cond)
        self.instr_out = Signal(16)
        self.offset_out = Signal(16)
        self.skip_size = Signal(ConstAddr)  # size of the instruction
        self.instr_size = Signal(ConstAddr)  # IP increment size
        self.change_main = Signal()  # the instruction modifies main operand
        self.change_offhand = Signal()  # the instruction modifies offhand operand
        self.change_ip = Signal()  # the instruction implicitly modifies IP

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        m.submodules.instr = instr = Latch(16)

        m.d.comb += instr.data_in.eq(self.instr_in)
        m.d.comb += instr.en_in.eq(self.en_in)
        m.d.comb += self.instr_out.eq(instr.data_out)

        with m.Switch(instr.data_out):
            with m.Case(DI.mask):
                m.d.comb += self.op_code_out.eq(instr.data_out[DI.op_code])
                m.d.comb += self.b_out.eq(instr.data_out[DI.byte])
                m.d.comb += self.mode_out.eq(instr.data_out[DI.mode])
                m.d.comb += self.reg_main_out.eq(instr.data_out[DI.reg_main])
                m.d.comb += self.reg_offhand_out.eq(
                    instr.data_out[DI.reg_offhand]
                )
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(0)

                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in DI.instructions if i.change_main]
                )):
                    m.d.comb += self.change_main.eq(1)
                    with m.If(self.reg_main_out == Reg.IP):
                        m.d.comb += self.change_ip.eq(1)

            with m.Case(DD.mask):
                m.d.comb += self.op_code_out.eq(instr.data_out[DD.op_code])
                m.d.comb += self.b_out.eq(instr.data_out[DD.byte])
                m.d.comb += self.mode_out.eq(0)
                m.d.comb += self.reg_main_out.eq(instr.data_out[DD.reg_main])
                m.d.comb += self.reg_offhand_out.eq(
                    instr.data_out[DD.reg_offhand]
                )
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(0)

                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in DD.instructions if i.change_main]
                )):
                    m.d.comb += self.change_main.eq(1)
                    with m.If(self.reg_main_out == Reg.IP):
                        m.d.comb += self.change_ip.eq(1)

                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in DD.instructions if i.change_offhand]
                )):
                    m.d.comb += self.change_offhand.eq(1)
                    with m.If(self.reg_main_out == Reg.IP):
                        m.d.comb += self.change_ip.eq(1)

            with m.Case(MIS.mask):
                m.d.comb += self.op_code_out.eq(instr.data_out[MIS.op_code])
                m.d.comb += self.b_out.eq(instr.data_out[MIS.byte])
                m.d.comb += self.mode_out.eq(instr.data_out[MIS.mode])
                m.d.comb += self.reg_main_out.eq(instr.data_out[MIS.reg_main])
                m.d.comb += self.reg_offhand_out.eq(0)
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(0)

                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in MIS.instructions if i.change_main]
                )):
                    m.d.comb += self.change_main.eq(1)

            with m.Case(MIW.mask):
                m.d.comb += self.op_code_out.eq(instr.data_out[MIW.op_code])
                m.d.comb += self.b_out.eq(0)
                m.d.comb += self.mode_out.eq(instr.data_out[MIW.mode])
                m.d.comb += self.reg_main_out.eq(instr.data_out[MIW.reg_main])
                m.d.comb += self.reg_offhand_out.eq(0)
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(0)

                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in MIW.instructions if i.change_main]
                )):
                    m.d.comb += self.change_main.eq(1)

                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in MIW.instructions if i.change_ip]
                )):
                    m.d.comb += self.change_ip.eq(1)

            with m.Case(MDS.mask):
                m.d.comb += self.op_code_out.eq(instr.data_out[MDS.op_code])
                m.d.comb += self.b_out.eq(instr.data_out[MDS.byte])
                m.d.comb += self.mode_out.eq(0)
                m.d.comb += self.reg_main_out.eq(instr.data_out[MDS.reg_main])
                m.d.comb += self.reg_offhand_out.eq(0)
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(0)

                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in MDS.instructions if i.change_main]
                )):
                    m.d.comb += self.change_main.eq(1)

            with m.Case(MDW.mask):
                m.d.comb += self.op_code_out.eq(instr.data_out[MDW.op_code])
                m.d.comb += self.b_out.eq(0)
                m.d.comb += self.mode_out.eq(0)
                m.d.comb += self.reg_main_out.eq(instr.data_out[MDW.reg_main])
                m.d.comb += self.reg_offhand_out.eq(0)
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(0)
                
                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in MDW.instructions if i.change_main]
                )):
                    m.d.comb += self.change_main.eq(1)

                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in MDW.instructions if i.change_ip]
                )):
                    m.d.comb += self.change_ip.eq(1)

            with m.Case(C.mask):
                m.d.comb += self.op_code_out.eq(instr.data_out[C.op_code])
                m.d.comb += self.b_out.eq(0)
                m.d.comb += self.mode_out.eq(0)
                m.d.comb += self.reg_main_out.eq(0)
                m.d.comb += self.reg_offhand_out.eq(0)
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.cond_out.eq(instr.data_out[C.cond])
                m.d.comb += self.offset_out.eq(0)

                m.d.comb += self.change_ip.eq(1)

            with m.Case(N.mask):
                m.d.comb += self.op_code_out.eq(instr.data_out[N.op_code])
                m.d.comb += self.b_out.eq(0)
                m.d.comb += self.mode_out.eq(0)
                m.d.comb += self.reg_main_out.eq(0)
                m.d.comb += self.reg_offhand_out.eq(0)
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(0)
                
                with m.If(self.op_code_out.matches(
                    *[i.op_code for i in C.instructions if i.change_ip]
                )):
                    m.d.comb +=  self.change_ip.eq(1)

            with m.Case(J.mask):
                m.d.comb += self.op_code_out.eq(0)
                m.d.comb += self.b_out.eq(0)
                m.d.comb += self.mode_out.eq(0)
                m.d.comb += self.reg_main_out.eq(0)
                m.d.comb += self.reg_offhand_out.eq(0)
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(Cat(
                    Const(0, shape=1),
                    instr.data_out[J.offset],
                    Repl(instr.data_out[J.offset][-1], 5),
                ))
                
                m.d.comb += self.change_ip.eq(1)

            with m.Default():
                m.d.comb += self.op_code_out.eq(0)
                m.d.comb += self.b_out.eq(0)
                m.d.comb += self.mode_out.eq(0)
                m.d.comb += self.reg_main_out.eq(0)
                m.d.comb += self.reg_offhand_out.eq(0)
                m.d.comb += self.cond_out.eq(0)
                m.d.comb += self.offset_out.eq(0)

        m.d.comb += self.skip_size.eq(ConstAddr.TWO)

        with m.If(self.mode_out == InstrMode.OFFSET):
            m.d.comb += self.skip_size.eq(ConstAddr.FOUR)

        with m.Elif(instr.data_out.matches(DI.mask)):
            with m.If(
                (self.mode_out == InstrMode.PREINCREMENT)
                & (self.reg_offhand_out == Reg.IP)
            ):
                m.d.comb += self.skip_size.eq(ConstAddr.FOUR)
            with m.Elif(
                (self.mode_out == InstrMode.POSTDECREMENT)
                & (self.reg_offhand_out == Reg.IP)
            ):
                m.d.comb += self.skip_size.eq(ConstAddr.ZERO)

        with m.Else():
            # MIS and MIW
            with m.If(
                (self.mode_out == InstrMode.PREINCREMENT)
                & (self.reg_main_out == Reg.IP)
            ):
                m.d.comb += self.skip_size.eq(ConstAddr.FOUR)
            with m.Elif(
                (self.mode_out == InstrMode.POSTDECREMENT)
                & (self.reg_main_out == Reg.IP)
            ):
                m.d.comb += self.skip_size.eq(ConstAddr.ZERO)

        with m.If(self.change_ip == 1):
            m.d.comb += self.instr_size.eq(ConstAddr.ZERO)
        with m.Else():
            m.d.comb += self.instr_size.eq(self.skip_size)

        return m

    def ports(self) -> List[Signal]:
        return [
            self.instr_in, self.en_in,
            self.op_code_out, self.b_out, self.mode_out,
            self.reg_main_out, self.reg_offhand_out, self.cond_out,
            self.instr_out, self.offset_out,
        ]
