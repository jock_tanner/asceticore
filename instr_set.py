from dataclasses import dataclass, InitVar
from typing import Iterable, Optional

from nmigen.tracer import get_var_name


__all__ = [
    'DI', 'DD', 'MIS', 'MIW', 'MDS', 'MDW', 'C', 'N', 'J',
    'Instruction', 'InstructionType',
]


@dataclass
class Instruction:

    mnemonic: str

    op_code: Optional[int]
    change_main: bool = True
    change_offhand: bool = False
    change_ip: bool = False
    doc: InitVar[str] = None

    def __post_init__(self, doc: str):
        self.__doc__ = doc


@dataclass
class InstructionType:

    mask: str
    name: str = None

    op_code: slice = None
    mode: slice = None
    byte: int = None
    reg_main: slice = None
    reg_offhand: slice = None
    cond: slice = None
    offset: slice = None

    instructions: Iterable[Instruction] = list

    doc: InitVar[str] = None

    def __post_init__(self, doc: str):
        self.__doc__ = doc
        if self.name is None:
            self.name = get_var_name(depth=3)

    def op(self, mnemonic: str) -> int:
        return next(
            i.op_code for i in self.instructions if i.mnemonic == mnemonic
        )


dyadic = [
    Instruction('AND', op_code=0b0000, doc='Bitwise AND'),
    Instruction('OR', op_code=0b0001, doc='Bitwise inclusive OR'),
    Instruction('XOR', op_code=0b0010, doc='Bitwise eXclusive OR'),
    Instruction('BRS', op_code=0b0011, doc='Bit ReSet'),
    Instruction('ADD', op_code=0b0100, doc='Arithmetic addition'),
    Instruction('ADC', op_code=0b0101, doc='Arithmetic addition with carry'),
    Instruction('SUB', op_code=0b0110, doc='Arithmetic subtraction'),
    Instruction(
        'SBB', op_code=0b0111,
        doc='Arithmetic subtraction with borrow',
    ),
    Instruction('MULS', op_code=0b1000, doc='Signed multiplication'),
    Instruction('MUL', op_code=0b1001, doc='Unsigned multiplication'),
    Instruction('DIVS', op_code=0b1010, doc='Signed division'),
    Instruction('DIV', op_code=0b1011, doc='Unsigned division'),
    Instruction(
        'CMP', op_code=0b1100, change_main=False,
        doc='Arithmetic comparison',
    ),
    Instruction('LOAD', op_code=0b1101, doc='Load register operand'),
    Instruction(
        'STORE', op_code=0b1110, change_main=False, change_offhand=True,
        doc='Store register operand',
    ),
    Instruction(
        'XCHG', op_code=0b1111, change_offhand=True,
        doc='Atomic exchange of operands',
    ),
]

sw_and_tail = [
    Instruction(
        'LDSW', op_code=0b100000,
        doc='Load status word',
    ),
    Instruction(
        'STSW', op_code=0b100001, change_main=False,
        doc='Store status word',
    ),
    Instruction(
        'BSSW', op_code=0b100010, change_main=False,
        doc='Set status word\'s bits',
    ),
    Instruction(
        'BRSW', op_code=0b100011, change_main=False,
        doc='Reset status word\'s bits',
    ),
    Instruction(
        'TAIL', op_code=0b100100,
        doc=(
            'Load higher part of multiplication product'
            ' or remainder of division'
        ),
    ),
]

calls = [
    Instruction(
        'CALL', 0b100000, change_main=False, change_ip=True,
        doc='Save IP to LR, then store operand in IP',
    ),
    Instruction(
        'RCALL', 0b100001, change_main=False, change_ip=True,
        doc='Save IP to LR, then add operand to IP',
    ),
]

DI = InstructionType(
    '1---------------', op_code=slice(11, 15), mode=slice(8, 10), byte=10,
    reg_main=slice(None, 4), reg_offhand=slice(4, 8), instructions=dyadic,
    doc='Dyadic indirect instructions',
)
DD = InstructionType(
    '000-------------', op_code=slice(9, 13), byte=8,
    reg_main=slice(None, 4), reg_offhand=slice(4, 8), instructions=dyadic,
    doc='Dyadic direct instructions',
)
MIS = InstructionType(
    '001-------------', op_code=slice(7, 13), mode=slice(4, 6), byte=6,
    reg_main=slice(None, 4),
    instructions=sw_and_tail,
    doc='Monadic indirect variable-sized instructions',
)
MIW = InstructionType(
    '0100------------', op_code=slice(6, 12), mode=slice(4, 6),
    reg_main=slice(None, 4),
    instructions=[
        Instruction('SEX', op_code=0b000000, doc='Signed extend byte to word'),
        Instruction('SWAB', op_code=0b000010, doc='Swap bytes'),
    ] + calls,
    doc='Monadic indirect word-sized instructions',
)
MDS = InstructionType(
    '01010-----------', op_code=slice(5, 11), byte=4,
    reg_main=slice(None, 4),
    instructions=[
        Instruction('ADC', op_code=0b000000, doc='Add carry to operand'),
        Instruction('INC', op_code=0b000001, doc='Add 1 to operand'),
        Instruction('ADD2', op_code=0b000010, doc='Add 2 to operand'),
        Instruction('ADD4', op_code=0b000011, doc='Add 4 to operand'),
        Instruction('SBB', op_code=0b000100, doc='Subtract borrow from operand'),
        Instruction('DEC', op_code=0b000101, doc='Subtract 1 from operand'),
        Instruction('SUB2', op_code=0b000110, doc='Subtract 2 from operand'),
        Instruction('SUB4', op_code=0b000111, doc='Subtract 4 from operand'),
        Instruction('SAR', op_code=0b001000, doc='Right arithmetic shift'),
        Instruction('SAL', op_code=0b001001, doc='Left arithmetic shift'),
        Instruction('SHR', op_code=0b001010, doc='Right logical shift'),
        Instruction('SHL', op_code=0b001011, doc='Left logical shift'),
        Instruction('RCR', op_code=0b001100, doc='Right roll through carry'),
        Instruction('RCL', op_code=0b001101, doc='Left roll through carry'),
        Instruction('ROR', op_code=0b001110, doc='Right cyclic shift'),
        Instruction('ROL', op_code=0b001111, doc='Left cyclic shift'),
        Instruction('NOT', op_code=0b010000, doc='Logical negation'),
        Instruction('NEG', op_code=0b010001, doc='Arithmetic negation'),
    ] + sw_and_tail,
    doc='Monadic direct variable-sized instructions',
)
MDW = InstructionType(
    '010110----------', op_code=slice(4, 10),
    reg_main=slice(None, 4),
    instructions=calls,
    doc='Monadic direct word-sized instructions',
)
C = InstructionType(
    '010111----------', op_code=slice(4, 10), cond=slice(None, 4),
    instructions=[
        Instruction('SKIP', op_code=0b000000, change_ip=True),
    ],
    doc='Conditional instructions',
)
N = InstructionType(
    '011000----------', op_code=slice(None, 10),
    doc='Niladic instructions',
)
J = InstructionType(
    '011111----------', offset=slice(None, 10),
    instructions=[
        Instruction('JUMP', op_code=None, change_ip=True),
    ],
    doc='Short relative jump with signed offset',
)
