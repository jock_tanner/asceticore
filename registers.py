from typing import List

from nmigen import Array, Cat, Elaboratable, Module, Signal
from nmigen.back.pysim import Simulator
from nmigen.build import Platform
from nmigen.cli import main_parser


class RegisterFile(Elaboratable):

    def __init__(self):

        # read port A
        self.ra_addr_in = Signal(4)
        self.ra_data_out = Signal(16)

        # read port B
        self.rb_addr_in = Signal(4)
        self.rb_data_out = Signal(16)

        # write port A (high priority)
        self.wa_enable_in = Signal()
        self.wa_byte_in = Signal()
        self.wa_addr_in = Signal(4)
        self.wa_data_in = Signal(16)

        # write port B (low priority)
        self.wb_enable_in = Signal()
        self.wb_byte_in = Signal()
        self.wb_addr_in = Signal(4)
        self.wb_data_in = Signal(16)

        # debug ports
        self.r0 = Signal(16)
        self.r1 = Signal(16)
        self.r2 = Signal(16)
        self.r3 = Signal(16)
        self.r4 = Signal(16)
        self.r5 = Signal(16)
        self.r6 = Signal(16)
        self.r7 = Signal(16)
        self.r8 = Signal(16)
        self.r9 = Signal(16)
        self.r10 = Signal(16)
        self.r11 = Signal(16)
        self.bp = Signal(16)
        self.sp = Signal(16)
        self.lr = Signal(16)
        self.ip = Signal(16)

        # registers
        self.regs_hi8 = Array(Signal(8) for _ in range(16))
        self.regs_lo8 = Array(Signal(8) for _ in range(16))

        # write conflict detection
        self.wr_addr_xored = Signal(4)
        self.write_conflict = Signal()

    def elaborate(self, platform: Platform) -> Module:

        m = Module()

        m.d.comb += self.ra_data_out.eq(
            Cat(
                self.regs_lo8[self.ra_addr_in],
                self.regs_hi8[self.ra_addr_in],
            )
        )
        m.d.comb += self.rb_data_out.eq(
            Cat(
                self.regs_lo8[self.rb_addr_in],
                self.regs_hi8[self.rb_addr_in],
            )
        )

        m.d.comb += self.wr_addr_xored.eq(self.wa_addr_in ^ self.wb_addr_in)
        m.d.comb += self.write_conflict.eq(
            ~(
                self.wr_addr_xored[0]
                | self.wr_addr_xored[1]
                | self.wr_addr_xored[2]
                | self.wr_addr_xored[3]
            ) & self.wa_enable_in & self.wb_enable_in
        )

        with m.If(self.wa_enable_in):
            m.d.sync += self.regs_lo8[self.wa_addr_in].eq(
                self.wa_data_in[:8]
            )
            with m.If(~self.wa_byte_in):
                m.d.sync += self.regs_hi8[self.wa_addr_in].eq(
                    self.wa_data_in[8:]
                )

        # write conflict resolves in favor of port A
        with m.If(self.wb_enable_in & ~self.write_conflict):
            m.d.sync += self.regs_lo8[self.wb_addr_in].eq(
                self.wb_data_in[:8]
            )
            with m.If(~self.wb_byte_in):
                m.d.sync += self.regs_hi8[self.wb_addr_in].eq(
                    self.wb_data_in[8:]
                )

        # debug
        m.d.comb += self.r0.eq(Cat(self.regs_lo8[0], self.regs_hi8[0]))
        m.d.comb += self.r1.eq(Cat(self.regs_lo8[1], self.regs_hi8[1]))
        m.d.comb += self.r2.eq(Cat(self.regs_lo8[2], self.regs_hi8[2]))
        m.d.comb += self.r3.eq(Cat(self.regs_lo8[3], self.regs_hi8[3]))
        m.d.comb += self.r4.eq(Cat(self.regs_lo8[4], self.regs_hi8[4]))
        m.d.comb += self.r5.eq(Cat(self.regs_lo8[5], self.regs_hi8[5]))
        m.d.comb += self.r6.eq(Cat(self.regs_lo8[6], self.regs_hi8[6]))
        m.d.comb += self.r7.eq(Cat(self.regs_lo8[7], self.regs_hi8[7]))
        m.d.comb += self.r8.eq(Cat(self.regs_lo8[8], self.regs_hi8[8]))
        m.d.comb += self.r9.eq(Cat(self.regs_lo8[9], self.regs_hi8[9]))
        m.d.comb += self.r10.eq(Cat(self.regs_lo8[10], self.regs_hi8[10]))
        m.d.comb += self.r11.eq(Cat(self.regs_lo8[11], self.regs_hi8[11]))
        m.d.comb += self.bp.eq(Cat(self.regs_lo8[12], self.regs_hi8[12]))
        m.d.comb += self.sp.eq(Cat(self.regs_lo8[13], self.regs_hi8[13]))
        m.d.comb += self.lr.eq(Cat(self.regs_lo8[14], self.regs_hi8[14]))
        m.d.comb += self.ip.eq(Cat(self.regs_lo8[15], self.regs_hi8[15]))

        return m

    def ports(self) -> List[Signal]:
        return [
            self.ra_addr_in, self.ra_data_out, self.rb_addr_in, self.rb_data_out,
            self.wa_enable_in, self.wa_byte_in, self.wa_addr_in, self.wa_data_in,
        ]


if __name__ == '__main__':

    def process():
        yield
        yield reg_file.ra_addr_in.eq(1)
        yield reg_file.rb_addr_in.eq(2)
        yield reg_file.wa_addr_in.eq(1)
        yield reg_file.wa_byte_in.eq(0)
        yield
        yield reg_file.wa_data_in.eq(0x1337)
        yield
        yield reg_file.wa_enable_in.eq(1)
        yield
        yield reg_file.wa_enable_in.eq(0)
        yield
        yield reg_file.wa_addr_in.eq(2)
        yield reg_file.wa_data_in.eq(0xd34d)
        yield
        yield reg_file.wa_enable_in.eq(1)
        yield
        yield
        yield reg_file.wa_enable_in.eq(0)
        yield
        yield reg_file.wa_addr_in.eq(1)
        yield reg_file.wa_data_in.eq(0xf00d)
        yield reg_file.wa_byte_in.eq(1)
        yield
        yield reg_file.wa_enable_in.eq(1)
        yield
        yield reg_file.wa_enable_in.eq(0)
        yield reg_file.wa_byte_in.eq(0)
        yield

    parser = main_parser()
    args = parser.parse_args()

    m = Module()
    m.submodules.reg_file = reg_file = RegisterFile()

    sim = Simulator(m)
    sim.add_clock(1e-6, domain='sync')
    sim.add_sync_process(process)

    with sim.write_vcd(
        'registers.vcd', 'registers.gtkw', traces=reg_file.ports(),
    ):
        sim.run()
