from nmigen import Cat, ClockDomain, ClockSignal, Elaboratable, Module
from nmigen.build import Platform

from altpll import AltPLLConfig, ALTPLL
from de0_nano import DE0NanoPlatform
from control import ControlUnit


class System(Elaboratable):

    def elaborate(self, platform: Platform) -> Module:

        # fake ROM
        fake_rom = {
            0x0000: 0b0101000000100000,  # INC R0
            0x0002: 0b0101000000000001,  # ADC R1
            0x0004: 0b0000010011111111,  # XOR IP, IP
        }

        m = Module()
        m.submodules.cpu = cpu = ControlUnit()

        main_clock = AltPLLConfig(multiply_by=2, divide_by=5)
        m.submodules.pll = pll = ALTPLL(plls=[main_clock])
        clk50 = platform.request(platform.default_clk, dir='-')
        m.d.comb += pll.inclk0.eq(clk50)
        m.domains += ClockDomain('sync')
        m.d.comb += ClockSignal('sync').eq(pll.clk_out[0])

        led_0 = platform.request('led', 0)
        led_1 = platform.request('led', 1)
        led_2 = platform.request('led', 2)
        led_3 = platform.request('led', 3)

        m.d.comb += led_0.eq(cpu.debug_out[7])
        m.d.comb += led_1.eq(cpu.debug_out[6])
        m.d.comb += led_2.eq(cpu.debug_out[5])
        m.d.comb += led_3.eq(cpu.debug_out[4])

        with m.If(cpu.req_out == 1):
            m.d.comb += cpu.grant_in.eq(1)

            with m.Switch(cpu.addr_out):
                for addr, data in fake_rom.items():
                    with m.Case(addr >> 1):
                        m.d.comb += cpu.data_in.eq(data)
                with m.Default():
                    m.d.comb += cpu.data_in.eq(0x0000)

        return m


if __name__ == '__main__':

    DE0NanoPlatform().build(System(), do_program=True)
